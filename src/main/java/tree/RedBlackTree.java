package tree;

import node.Color;
import node.Node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

public class RedBlackTree<K extends Comparable<K>, V> implements BinaryTree<K, V> {
    private final Node<K, V> nil = new Node<>(Color.BLACK);
    private Node<K, V> root = nil;
    private int size = 0;

    @Override
    public void add(K key, V value) {
        Node<K, V> newNode = new Node<>(key, value);
        Node<K, V> current = root;
        Node<K, V> previous = nil;
        int comparison = 0;
        while (current != nil) {
            previous = current;
            comparison = newNode.compareTo(current);
            if (comparison == 0) {
                current.setValue(newNode.getValue());
                return;
            }

            if (comparison < 0) {
                current = current.left;
            } else {
                current = current.right;
            }
        }

        newNode.parent = previous;
        if (previous == nil) {
            root = newNode;
        } else if (comparison < 0) {
            previous.left = newNode;
        } else {
            previous.right = newNode;
        }
        newNode.color = Color.RED;
        newNode.left = nil;
        newNode.right = nil;
        Operations.balanceInsert(newNode, this);
        root.color = Color.BLACK;
        size++;
    }

    @Override
    public V remove(K key) {
        Node<K, V> oldNode = getNode(key);
        Node<K, V> newNode;
        Node<K, V> newNodeChild;
        if (oldNode == null) {
            return null;
        }
        size--;

        Color oldColor = oldNode.color;
        if (oldNode.left == nil) {
            newNode = oldNode.right;
            Operations.transplant(oldNode, newNode, this);
        } else if (oldNode.right == nil) {
            newNode = oldNode.left;
            Operations.transplant(oldNode, newNode, this);
        } else {
            newNode = Operations.getMinimum(oldNode.right, this);
            oldColor = newNode.color;
            newNodeChild = newNode.right;
            if (newNode.parent == oldNode) {
                newNodeChild.parent = newNode;
            } else {
                Operations.transplant(newNode, newNodeChild, this);
                newNode.right = oldNode.right;
                newNode.right.parent = newNode;
            }
            Operations.transplant(oldNode, newNode, this);
            newNode.left = oldNode.left;
            newNode.left.parent = newNode;
            newNode.color = oldNode.color;
            newNode = newNodeChild;
        }
        if (oldColor == Color.BLACK) {
            Operations.balanceRemoval(newNode, this);
        }
        return oldNode.getValue();
    }

    @Override
    public V get(K key) {
        Node<K, V> node = getNode(key);
        return node == null ? null : node.getValue();
    }

    @Override
    public boolean isEmpty() {
        return root == nil;
    }

    @Override
    public int size() {
        return size;
    }

    private Node<K, V> getNode(K key) {
        Node<K, V> current = root;
        int comparison;
        while (current != nil) {
            comparison = key.compareTo(current.getKey());
            if (comparison == 0) {
                return current;
            } else if (comparison < 0) {
                current = current.left;
            } else {
                current = current.right;
            }
        }
        return null;
    }

    Node<K, V> getNil() {
        return nil;
    }

    Node<K, V> getRoot() {
        return root;
    }

    void setRoot(Node<K, V> root) {
        this.root = root;
    }

    @Override
    public Iterator<V> iterator() {
        return new TreeIterator(root);
    }

    private class TreeIterator implements Iterator<V> {

        private final Queue<Node<K, V>> queue = new LinkedList<>();
        private TreeIterator(Node<K, V> root) {
            if (root != null) {
                queue.add(root);
            }
        }

        @Override
        public boolean hasNext() {
            return !queue.isEmpty();
        }

        @Override
        public V next() {
            if (!hasNext()) {
                throw new NoSuchElementException("No next element");
            }
            Node<K, V> ret = queue.remove();
            if (ret.left != nil) {
                queue.add(ret.left);
            }
            if (ret.right != nil) {
                queue.add(ret.right);
            }
            return ret.getValue();
        }

        @Override
        public String toString() {
            Iterator<V> iterator = iterator();
            StringBuilder sb = new StringBuilder();
            while (iterator.hasNext()) {
                sb.append(String.format("%s ", iterator.next()));
            }
            return sb.toString();
        }
    }
}
