package tree;

import node.Color;
import node.Node;

class Operations {

    private static <K extends Comparable<K>, V> void leftRotate(Node<K, V> x, RedBlackTree<K, V> tree) {
        Node<K, V> y = x.right;
        x.right = y.left;

        if (y.left != tree.getNil()) {
            y.left.parent = x;
        }

        y.parent = x.parent;
        if (x.parent == tree.getNil()) {
            tree.setRoot(y);
        } else if (x == x.parent.left) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }

        y.left = x;
        x.parent = y;
    }

    private static <K extends Comparable<K>, V> void rightRotate(Node<K, V> y, RedBlackTree<K, V> tree) {
        Node<K, V> x = y.left;
        y.left = x.right;

        if (x.right != tree.getNil()) {
            x.right.parent = y;
        }

        x.parent = y.parent;
        if (y.parent == tree.getNil()) {
            tree.setRoot(x);
        } else if (y == y.parent.left) {
            y.parent.left = x;
        } else {
            y.parent.right = x;
        }

        x.right = y;
        y.parent = x;
    }

    static <K extends Comparable<K>, V> void balanceInsert(Node<K, V> newNode, RedBlackTree<K, V> tree) {
        Node<K, V> current = newNode;
        Node<K, V> uncle;
        Node<K, V> grandpa;
        while (current.parent.color == Color.RED) {
            grandpa = current.parent.parent;
            if (current.parent == grandpa.left) {
                uncle = grandpa.right;
                if (uncle.color == Color.RED) {
                    current = changeColors(current.parent, uncle, grandpa);
                } else {
                    current = rotate(current, true, tree);
                }
            } else {
                uncle = grandpa.left;
                if (uncle.color == Color.RED) {
                    current = changeColors(current.parent, uncle, grandpa);
                } else {
                    current = rotate(current, false, tree);
                }
            }
        }
    }

    private static <K extends Comparable<K>, V> Node<K, V> changeColors(Node<K, V> father,
                                                                        Node<K, V> uncle, Node<K, V> grandpa) {
        father.color = Color.BLACK;
        uncle.color = Color.BLACK;
        grandpa.color = Color.RED;
        return grandpa;
    }

    private static <K extends Comparable<K>, V> Node<K, V> rotate(Node<K, V> current,
                                                                  boolean isLeftBrunch, RedBlackTree<K, V> tree) {
        if (isLeftBrunch && current == current.parent.right) {
            current = current.parent;
            leftRotate(current, tree);
        }
        if (!isLeftBrunch && current == current.parent.left) {
            current = current.parent;
            rightRotate(current, tree);
        }

        current.parent.color = Color.BLACK;
        current.parent.parent.color = Color.RED;

        if (isLeftBrunch) {
            rightRotate(current.parent.parent, tree);
        } else {
            leftRotate(current.parent.parent, tree);
        }
        return current;
    }

    static <K extends Comparable<K>, V> void balanceRemoval(Node<K, V> newNode, RedBlackTree<K, V> tree) {
        Node<K, V> sibling;
        while (newNode != tree.getRoot() && newNode.color == Color.BLACK) {
            if (newNode == newNode.parent.left) {
                sibling = newNode.parent.right;
                System.out.println("Left brunch, sibling : " + sibling.toString());
                System.out.println("Left brunch, parent : " + newNode.parent.toString());
                if (sibling.color == Color.RED) {
                    sibling = makeSiblingParent(sibling, newNode, tree, true);
                }
                if (sibling.left.color == Color.BLACK && sibling.right.color == Color.BLACK) {
                    sibling.color = Color.RED;
                    newNode = newNode.parent;
                } else {
                    if (sibling.right.color == Color.BLACK) {
                        sibling = rotateSibling(sibling, newNode, tree, true);
                    }
                    newNode = rotateParent(sibling, newNode, tree, true);
                }
            } else {
                sibling = newNode.parent.left;
                System.out.println("Right brunch, sibling : " + sibling.toString());
                System.out.println("Right brunch, parent : " + newNode.parent.toString());
                if (sibling.color == Color.RED) {
                    sibling = makeSiblingParent(sibling, newNode, tree, false);
                }
                if (sibling.left.color == Color.BLACK && sibling.right.color == Color.BLACK) {
                    sibling.color = Color.RED;
                    newNode = newNode.parent;
                } else {
                    if (sibling.left.color == Color.BLACK) {
                        sibling = rotateSibling(sibling, newNode, tree, false);
                    }
                    newNode = rotateParent(sibling, newNode, tree, false);
                }
            }
        }
        newNode.color = Color.BLACK;
    }

    private static <K extends Comparable<K>, V> Node<K, V> makeSiblingParent(Node<K, V> sibling, Node<K, V> newNode,
                                                                             RedBlackTree<K, V> tree, boolean isLeftBrunch) {
        sibling.color = Color.BLACK;
        newNode.parent.color = Color.RED;
        if (isLeftBrunch) {
            leftRotate(newNode.parent, tree);
            return newNode.parent.right;
        }
        rightRotate(newNode.parent, tree);
        return newNode.parent.left;
    }

    private static <K extends Comparable<K>, V> Node<K, V> rotateSibling(Node<K, V> sibling, Node<K, V> newNode,
                                                                         RedBlackTree<K, V> tree, boolean isLeftBrunch) {
        sibling.color = Color.RED;
        if (isLeftBrunch) {
            sibling.left.color = Color.BLACK;
            rightRotate(sibling, tree);
            return newNode.parent.right;
        }
        sibling.right.color = Color.BLACK;
        leftRotate(sibling, tree);
        return newNode.parent.left;
    }

    private static <K extends Comparable<K>, V> Node<K, V> rotateParent(Node<K, V> sibling, Node<K, V> newNode,
                                                                        RedBlackTree<K, V> tree, boolean isLeftBrunch) {
        sibling.color = newNode.parent.color;
        newNode.parent.color = Color.BLACK;
        if (isLeftBrunch) {
            sibling.right.color = Color.BLACK;
            leftRotate(newNode.parent, tree);
        } else {
            sibling.left.color = Color.BLACK;
            rightRotate(newNode.parent, tree);
        }
        return tree.getRoot();
    }

    static <K extends Comparable<K>, V> void transplant(Node<K, V> oldNode,
                                                        Node<K, V> newNode, RedBlackTree<K, V> tree) {
        newNode.parent = oldNode.parent;

        if (oldNode.parent == tree.getNil()) {
            tree.setRoot(newNode);
        } else if (oldNode.parent.left == oldNode) {
            oldNode.parent.left = newNode;
        } else {
            oldNode.parent.right = newNode;
        }
    }

    static <K extends Comparable<K>, V> Node<K, V> getMinimum(Node<K, V> root, RedBlackTree<K, V> tree) {
        while (root.left != tree.getNil()) {
            root = root.left;
        }
        return root;
    }
}
