package tree;


public interface BinaryTree<K, V> extends Iterable<V> {
    void add(K key, V value);
    V remove(K key);
    V get(K key);
    int size();
    boolean isEmpty();
}
