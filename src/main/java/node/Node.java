package node;

public class Node<K extends Comparable<K>, V> implements Comparable<Node<K, V>> {
    public Node<K, V> left;
    public Node<K, V> right;
    public Node<K, V> parent;
    public Color color;
    private K key;
    private V value;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public Node(Color color) {
        this.color = color;
    }

    public int compareTo(Node<K, V> o) {
        return key.compareTo(o.getKey());
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\nMe : %s", key));
        if (parent != null) {
            sb.append(String.format("\nParent : %s", parent.getKey()));
        }
        if (left != null) {
            sb.append(String.format("\nLeft : %s", left.getKey()));
        }
        if (right != null) {
            sb.append(String.format("\nRight : %s", right.getKey()));
        }
        sb.append(String.format("\nColor : %s", color));
        return sb.toString();
    }
}
