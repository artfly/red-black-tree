import tree.BinaryTree;
import tree.RedBlackTree;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        BinaryTree<Integer, Integer> tree = new RedBlackTree<>();
        Set<Integer> numbers = new HashSet<>();
        int nextValue;
        for (int i = 0; i < 60; i++) {
            nextValue = ThreadLocalRandom.current().nextInt(0, 100);
            tree.add(nextValue, nextValue);
            numbers.add(nextValue);
        }
        numbers.forEach(tree::remove);
        if (tree.isEmpty()) {
            System.out.println("Removed!");
        }
    }
}
